import React from "react";
import Header from "./components/header";
import MainContent from "./components/mainContent";
import DateState from "./context/dataState";
import Alert from "./components/alert";

function App() {
  const [alert, setAlert] = React.useState(true);
  // "Use Desktop browser for Better Experience"

  setTimeout(() => {
    setAlert(false);
  }, 2000);
  return (
    <DateState>
      <div className="App">
        {alert ? (
          <Alert message={"Use Desktop browser for Better Experience"} />
        ) : null}
        <Header />
        <MainContent />
      </div>
    </DateState>
  );
}

export default App;
