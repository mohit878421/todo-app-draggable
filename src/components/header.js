import React from "react";
import DataContext from "../context/dataContext";

let Header = function (props) {
  let datasContext = React.useContext(DataContext);
  let { submit } = datasContext;
  const [modalIsOpen, setModalIsOpen] = React.useState(false);
  let model = function () {
    setModalIsOpen(modalIsOpen == true ? false : true);
  };

  let [title, setTitle] = React.useState("");
  let [dec, setDec] = React.useState("");
  let onChangeTitle = (e) => {
    setTitle(e.target.value);
  };

  let onChangeDec = (e) => {
    setDec(e.target.value);
  };
  return (
    <div className="header">
      <h2 className="heading">TODO APP</h2>
      <div className="form">
        <label className="label">
          Title:
          <input
            className="formField"
            type="text"
            value={title}
            onChange={onChangeTitle}
          />
        </label>
        <label className="label">
          Description:
          <input
            className="formField"
            type="text"
            value={dec}
            onChange={onChangeDec}
          />
        </label>
        <button onClick={() => submit(title, dec)}>Add Task</button>
      </div>
    </div>
  );
};

export default Header;
