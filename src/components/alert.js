import React from "react";

const Alert = function (props) {
  return <div className="alert">{props.message}</div>;
};
export default Alert;
