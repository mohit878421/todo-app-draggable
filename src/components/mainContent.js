import React from "react";
import Box from "./box";
import DataContext from "../context/dataContext";

let dragItemId = null;
let dragItemType = null;
let targetType = null;
let MainContent = function (props) {
  let [searchItem, setSearchItem] = React.useState();
  let datasContext = React.useContext(DataContext);
  let { pendingWork, progressWork, workCompleted, swipe, search, searchItems } =
    datasContext;

  let dragStart = function (type, index) {
    debugger;
    return () => {
      dragItemType = type;
      dragItemId = index;
    };
  };
  let onDragOver = function (type) {
    debugger
    return (e) => {
      targetType = type;
    };
  };

  let onDragEnd = function () {
    return swipe(dragItemType, dragItemId, targetType);
  };
  return (
    <div className="mainContent">
      <div className="group">
        <h3 className="heading">Search Item</h3>
        <div className="searchBox">
          <input
            className="formField"
            type="text"
            value={searchItem}
            onChange={(e) => setSearchItem(e.target.value)}
          />
          <button onClick={() => search(searchItem)}>Search</button>
        </div>
        {searchItems.map((item, index) => {
          return (
            <Box
              title={item.title}
              des={item.des}
              dragStart={dragStart("pendingWork", index)}
              onDragEnd={onDragEnd}
            />
          );
        })}
      </div>
      <div className="group" onDragOver={onDragOver("pendingWork")}>
        <h3 className="heading">Pending Work </h3>
        {pendingWork.map((item, index) => {
          return (
            <Box
              title={item.title}
              des={item.des}
              dragStart={dragStart("pendingWork", index)}
              onDragEnd={onDragEnd}
            />
          );
        })}
      </div>
      <div className="group" onDragOver={onDragOver("progressWork")}>
        <h3 className="heading">Work in Progress</h3>
        {progressWork.map((item, index) => {
          return (
            <Box
              title={item.title}
              des={item.des}
              dragStart={dragStart("progressWork", index)}
              onDragEnd={onDragEnd}
            />
          );
        })}
      </div>
      <div className="group" onDragOver={onDragOver("workCompleted")}>
        <h3 className="heading">Work Completed</h3>
        {workCompleted.map((item, index) => {
          return (
            <Box
              title={item.title}
              des={item.des}
              dragStart={dragStart("workCompleted", index)}
              onDragEnd={onDragEnd}
            />
          );
        })}
      </div>
    </div>
  );
};

export default MainContent;
