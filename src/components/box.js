import React from "react";
import "../App.css";

let Box = function (props) {
  return (
    <div draggable className="item" onDragStart={props.dragStart} onDragEnd={props.onDragEnd}>
      <div className="box">Title : {props.title}</div>
      <div className="box">Des : {props.des}</div>
    </div>
  );
};

export default Box;
