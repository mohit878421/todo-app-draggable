import DataContext from "./dataContext";
import React from "react";

const DataState = function (props) {
  let [pendingWork, setPendingWork] = React.useState([
    { title: "Collage Work3", des: "Learn Nest" },
  ]);

  let [progressWork, setProgressWork] = React.useState([
    { title: "Collage Work2", des: "Learn MYSQL" },
  ]);

  let [workCompleted, setWorkCompleted] = React.useState([
    { title: "Collage Work1", des: "Learn NoSQL" },
  ]);

  let [searchItems, setSearchItem] = React.useState([]);

  let mapArray = {
    pendingWork: pendingWork,
    progressWork: progressWork,
    workCompleted: workCompleted,
  };
  
  let mapArrayStates = {
    pendingWork: setPendingWork,
    progressWork: setProgressWork,
    workCompleted: setWorkCompleted,
  };
  let submit = function (title, des) {
    console.log(title, des);
    if (!title || !des) {
      return;
    }
    let clonePendingWork = [...pendingWork];
    clonePendingWork.push({ title, des });
    debugger;
    setPendingWork(clonePendingWork);
  };

  let swipe = function (getArray, itemIndex, pushArray) {
    if (getArray == pushArray) {
      return;
    }

    let getItemArray = [...mapArray[getArray]];
    let getItem = getItemArray[itemIndex];
    getItemArray.splice(itemIndex, 1);
    let state = mapArrayStates[getArray];
    state(getItemArray);

    let pushItemArray = [...mapArray[pushArray]];
    pushItemArray.push(getItem);
    let statePush = mapArrayStates[pushArray];
    statePush(pushItemArray);
  };

  let search = function (title) {
    for (let i = 0; i <= pendingWork.length - 1; i++) {
      if (pendingWork[i].title == title) {
        let CloneArray = [...searchItems];
        CloneArray.push(pendingWork[i]);
        setSearchItem(CloneArray);
      }
    }

    for (let i = 0; i <= progressWork.length - 1; i++) {
      if (progressWork[i].title == title) {
        let CloneArray = [...searchItems];
        CloneArray.push(progressWork[i]);
        setSearchItem(CloneArray);
      }
    }

    for (let i = 0; i <= workCompleted.length - 1; i++) {
      if (workCompleted[i].title == title) {
        let CloneArray = [...searchItems];
        CloneArray.push(workCompleted[i]);
        setSearchItem(CloneArray);
      }
    }
  };

  return (
    <DataContext.Provider
      value={{
        pendingWork,
        progressWork,
        workCompleted,
        submit,
        swipe,
        search,
        searchItems,
      }}
    >
      {props.children}
    </DataContext.Provider>
  );
};

export default DataState;
